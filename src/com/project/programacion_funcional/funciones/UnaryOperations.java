package com.project.programacion_funcional.funciones;

import com.project.programacion_funcional.utils.Estudiante;
import com.project.programacion_funcional.utils.UtilTest;

import java.util.Locale;
import java.util.function.UnaryOperator;

public class UnaryOperations {

    static UtilTest util = new UtilTest();
    public static void main(String[] args) {

        //Extiende de Function, con la diferencia que retorna el mismo tipo de dato que recibe.
        UnaryOperator<String> mayusName = name -> name.toUpperCase(Locale.ROOT);

        util.estudianteList.forEach(estudiante ->
                System.out.println(
                        mayusName.apply(estudiante.getName()))
        );

    }
}
