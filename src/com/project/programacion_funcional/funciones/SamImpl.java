package com.project.programacion_funcional.funciones;

import com.project.programacion_funcional.utils.SamGenericInterface;
import com.project.programacion_funcional.utils.SamInterface;

public class SamImpl {

    static SamInterface suma = (x,y) -> x+y;
    static SamInterface multi = (x,y) -> x*y;

    public static void main(String[] args) {

        System.out.println(suma.operation(5,6));
        System.out.println(multi.operation(4,5));



    }


}
