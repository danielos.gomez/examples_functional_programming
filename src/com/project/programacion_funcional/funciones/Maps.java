package com.project.programacion_funcional.funciones;

import com.project.programacion_funcional.utils.Estudiante;
import com.project.programacion_funcional.utils.UtilTest;

import java.util.DoubleSummaryStatistics;

public class Maps {

   static UtilTest util = new UtilTest();

    public static void main(String[] args) {
        //sumando todos los puntajes con REDUCE
        util.estudianteList.stream()
                .map(Estudiante::getPuntaje)
                .reduce((x,y)->x+y).ifPresent(System.out::println);


        //Calcular puntaje promedio con MAPINT
        util.estudianteList.stream().
                mapToInt(x-> (int) x.getPuntaje())
                .average().ifPresent(System.out::println);

       DoubleSummaryStatistics estadistica = util.estudianteList.stream().
                mapToDouble(Estudiante::getPuntaje)
                .summaryStatistics();

       //mejor puntaje
        System.out.println(estadistica.getMax());

        //peor puntaje
        System.out.println(estadistica.getMin());


    }

}
