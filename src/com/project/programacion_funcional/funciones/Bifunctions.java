package com.project.programacion_funcional.funciones;

import java.util.function.BiFunction;

public class Bifunctions {
    public static void main(String[] args) {

        //1 y 2 parametros : tipos de datos de lo que se va a operar
        // 3 parametro tipo de dato que retorna
        BiFunction<Integer,Integer,Integer> multiplication = (x,y)-> x*y;
        BiFunction<Integer,Integer,Integer> suma = (x,y)-> x+y;
        

        System.out.println(multiplication.apply(3,4));
    }
}
