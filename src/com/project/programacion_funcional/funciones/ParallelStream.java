package com.project.programacion_funcional.funciones;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ParallelStream {

    public static void main(String[] args) {

        Supplier<IntStream> supplier = ()-> IntStream.iterate(0,x-> x+1);

        //IntStream intStream = IntStream.iterate(0,x->x+1);

        //Paralela en threads
        System.out.println("PARALELA");
        supplier.get().parallel().limit(20).filter(x->x%2==0).forEach(System.out::println);

        System.out.println("_____________________________");
        // lineal en un solo threads
        System.out.println("LINEAL");

        supplier.get().limit(20).filter(x->x%2==0).forEach(System.out::println);

        //con Boxed convierto un InsStream en un Stream para poderlo Collectar
       List<Integer> listNumber = supplier.get().
                limit(20).
                filter(x->x%2==0)
                .boxed()
                .collect(Collectors.toList());


    }
}
