package com.project.programacion_funcional.funciones;

import com.project.programacion_funcional.utils.UtilTest;
import java.util.function.Supplier;


public class Suppliers {

    // retorna un valor sin recibir ningun parametro
    static UtilTest util =  new UtilTest();
    public static void main(String[] args) {

        Supplier<Integer> printResults =
                () -> (int)Math.floor(Math.random()*100)+1;

        System.out.println(printResults.get());

    }
}
