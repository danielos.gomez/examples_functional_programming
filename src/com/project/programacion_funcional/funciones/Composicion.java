package com.project.programacion_funcional.funciones;

import java.util.function.Function;

public class Composicion {


    static Function<Integer,Integer> duplicar= (x) -> {
        System.out.println("duplicando:"+x);
        return x*2;

    };

    static Function<Integer,Integer> triplicar=
            duplicar.compose(y ->{
                System.out.println("triplicando:"+y);
                return y*3;
            });

    static Function<Integer,Integer> quintiplicar=
            triplicar.andThen(y ->{
                System.out.println("quintiplicando:"+y);
                return y*5;
            });

    public static void main(String[] args) {

        //System.out.println("Resultado triplicando: "+triplicar.apply(5));

        System.out.println("Resultado quintiplicando: "+quintiplicar.apply(5));
    }




}
