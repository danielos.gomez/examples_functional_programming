package com.project.programacion_funcional.funciones;

import com.project.programacion_funcional.utils.UtilTest;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class ReferenceOperator {


    static <T> List<T> listGeneric (T ... elements) {
        return List.of(elements);
    }

    public static void main(String[] args) {
        List<String> listNombres = listGeneric("Daniel" , "Camilo" , "Elvis");

        Consumer consumer = x -> System.out.println(x);

        listNombres.forEach(consumer);


        for(int i = 0; i<listNombres.size();i++){
            System.out.println(listNombres.get(i));
        }

        listNombres.forEach(item -> System.out.println(item));
        listNombres.forEach(System.out::println);
        listNombres.forEach(UtilTest::printObject);


        /*
        Operador de Referencia : se utiliza para llamar a un método refiriéndose a él directamente
        con la ayuda de su clase

           -Un método estático:(ClassName::methodName)
           -Un método de instancia:(objectOfClass::methodName)
           -Un constructor:(super::methodName)
        */


    }




}
