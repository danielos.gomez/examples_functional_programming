package com.project.programacion_funcional.funciones;

import com.project.programacion_funcional.utils.Estudiante;
import com.project.programacion_funcional.utils.UtilTest;

import java.util.List;
import java.util.function.Consumer;

public class Consumers {

    static UtilTest util = new UtilTest();

    public static void main(String[] args) {
/*
Un consumer recibe un parametro y ejecuta algun procedimiento, pero no retorna nada
 */
        Consumer<List<Estudiante>> printResults =
                item -> item.stream().forEach(estudiante ->
                        System.out.println(estudiante.getName()+":"+estudiante.getPuntaje()));

        printResults.accept(util.estudianteList);


    }
}
