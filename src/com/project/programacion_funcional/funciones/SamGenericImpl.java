package com.project.programacion_funcional.funciones;

import com.project.programacion_funcional.utils.SamGenericInterface;

import java.time.LocalDate;
import java.time.Period;
import java.util.function.Function;

public class SamGenericImpl {

    static SamGenericInterface<String,String,String,LocalDate> generateDate =
            (day,month,year) -> LocalDate.parse(year+"-"+month+
            "-"+day);

    //agrega un 0 si el digito recibido es < 10
    static Function<Integer,String> converDigit = (x)-> x<10 ?"0"+x.toString():x.toString();

    static Function<LocalDate, Integer> getAge = (x)-> Period.between(x,LocalDate.now()).getYears();

    public static void main(String[] args) {
        System.out.println(generateDate.
                apply(
                        converDigit.apply(4),
                        converDigit.apply(4),
                        converDigit.apply(1994)
                ));
        System.out.println(LocalDate.now());

        System.out.println(getAge.apply(
                generateDate.apply(
                        converDigit.apply(4),
                        converDigit.apply(4),
                        converDigit.apply(1994))));
    }

}
