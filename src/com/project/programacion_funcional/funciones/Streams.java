package com.project.programacion_funcional.funciones;

import com.project.programacion_funcional.utils.Estudiante;
import com.project.programacion_funcional.utils.UtilTest;

import java.util.Comparator;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class Streams {

   static UtilTest util = new UtilTest();

    static Supplier<Stream<Estudiante>> generadorStream = () -> util.estudianteList.stream();

    public static void main(String[] args) {


        //ejemplo obteniendo tamaño de los nombres
        Stream<Integer> lengthNameSteam = generadorStream.get().map((x)-> x.getName().length());

        //obteniendo estudiantes aprobados con filter
        Stream<Estudiante> estudiantesAprobados = generadorStream.get().filter((estudiante -> estudiante.getPuntaje()>=3));

        //Ordenar por nombre con Sorted
        generadorStream.get().sorted(Comparator.comparing(Estudiante::getName)).forEach(estudiant -> System.out.println(estudiant.getName()));

    }



}
