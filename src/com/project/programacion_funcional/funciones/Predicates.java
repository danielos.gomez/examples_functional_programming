package com.project.programacion_funcional.funciones;

import com.project.programacion_funcional.utils.Estudiante;
import com.project.programacion_funcional.utils.UtilTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class Predicates {

    static UtilTest util = new UtilTest();

    public static void main(String[] args) {

           /*
        UN PREDICADO VALIDA ALGO Y RETORNA TRUE O FALSE
         */

        //PREDICADO QUE RETORNA SI UN NUMERO ES PAR

        Predicate<Integer> esPar = (x) -> x%2 == 0;
        System.out.println(esPar.test(13));



        //PREDICADO QUE RECIBE LA CLASE ESTUDIANTE Y RETORNA SI APROBO O NO
        Predicate<Estudiante> aproboMateria = (e) -> e.getPuntaje()>=3;


        util.estudianteList.stream().forEach
                (e -> System.out.println(e.getName()+":"+aproboMateria.test(e)));
    }
}
