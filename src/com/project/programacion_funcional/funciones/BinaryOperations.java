package com.project.programacion_funcional.funciones;

import java.util.function.BinaryOperator;

public class BinaryOperations {
    public static void main(String[] args) {

        // Recibe dos parametros del mismo tipo de dato y retorna un valor del mismo tipo de dato
        BinaryOperator<Integer> multi = (x,y) -> x*y;
        System.out.println(multi.apply(3,5));
    }
}
