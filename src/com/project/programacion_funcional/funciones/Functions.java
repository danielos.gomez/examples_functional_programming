package com.project.programacion_funcional.funciones;

import java.util.function.Function;

public class Functions {

    public static void main(String[] args) {

        //FUNCION QUE RETORNA EL CUADRADO
        Function<Integer, Integer> potencia =
                new Function<Integer, Integer>() {
            @Override
            public Integer apply(Integer integer) {
                return integer*integer;
            }
        };
        System.out.println(potencia.apply(5));


        //FUNCION QUE RETORNA SI UN NUMERO ES PAR
        Function<Integer,Boolean> esPar = (x) -> x%2 == 0;
        System.out.println(esPar.apply(2));






    }
}
