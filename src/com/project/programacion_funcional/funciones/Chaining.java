package com.project.programacion_funcional.funciones;


public class Chaining {
     Integer valor;

    public Chaining increment(){
        valor+=1;
        return this;
    }

    public Chaining decrement(){
        valor-=1;
        return this;
    }

    public void constructor(int x) {
        this.valor= x;
    }

    public static void main(String[] args) {
        Chaining prueba = new Chaining();
        prueba.constructor(5);
        prueba.increment().increment().increment().increment();
        System.out.println(prueba.valor);

    }
}

