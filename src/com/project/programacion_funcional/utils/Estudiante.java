package com.project.programacion_funcional.utils;

public class Estudiante {
    String name;
    double puntaje;
    boolean puntajePrinter;

    public Estudiante(){

    }

    public Estudiante(double puntaje , String name , boolean puntajePrinter) {
        this.puntaje = puntaje;
        this.name = name;
        this.puntajePrinter = puntajePrinter;
    }


    public double getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(double puntaje) {
        this.puntaje = puntaje;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName (){
        return name;
    }

    public boolean isPuntajePrinter(boolean b) {
        return puntajePrinter;
    }

    public void setPuntajePrinter(boolean puntajePrinter) {
        this.puntajePrinter = puntajePrinter;
    }

}
