package com.project.programacion_funcional.utils;

import java.util.Arrays;
import java.util.List;

public class UtilTest {

    public final List<Estudiante> estudianteList =
            Arrays.asList(
                    new Estudiante(4.9,"Daniel", false),
                    new Estudiante(5.0,"Mabel", false),
                    new Estudiante(3,"Esteban", false),
                    new Estudiante(1.9,"Katherine", false),
                    new Estudiante(2,"Luis", false),
                    new Estudiante(4.9,"Daniel", false)
            );



    public static <T> void printObject(T algo){
        System.out.println(algo);
    }

    }

