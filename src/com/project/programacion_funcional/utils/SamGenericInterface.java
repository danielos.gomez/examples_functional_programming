package com.project.programacion_funcional.utils;

@FunctionalInterface
public interface SamGenericInterface <A,B,C,R>{
    R apply (A a,B b,C c);
}
