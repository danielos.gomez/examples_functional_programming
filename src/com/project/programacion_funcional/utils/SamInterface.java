package com.project.programacion_funcional.utils;

/*
//SAM : Single Abstract Method//

La Etiqueta @FunctionalInterface valida que solo exista un unico metodo en la interface,
si se crea otro metodo por accidente generaria una exception
 */

@FunctionalInterface
public interface SamInterface {

    public Integer operation(int x , int y);

}
