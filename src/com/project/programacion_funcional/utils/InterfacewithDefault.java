package com.project.programacion_funcional.utils;

@FunctionalInterface
public interface InterfacewithDefault {
    Integer getCant();

    default void multiplicacion(Integer num){
        for (int i=1; i<=getCant(); i++){
            System.out.println(num+"*"+i +"="+ num*i);
        }
    }

    default void suma(Integer num){
        for (int i=1; i<=getCant(); i++){
            System.out.println(num+"+"+i +"="+ (num+i));
        }
    }
}
